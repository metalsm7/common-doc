# IA

## FrontEnd / 사용자

Depth 1 | Depth 2 | Depth 3 | 로그인 | desc
---|---|---|---|---
home | | | | 프로그래밍<br />&nbsp;&nbsp;- 최신뉴스<br />&nbsp;&nbsp;- 카운터
개인정보취급방침
회원약관
제품/솔루션
&nbsp; | Display<br />Panel<br />Inspection
&nbsp; | Smart<br />Computing<br />Solution
&nbsp; | | Panel PC
&nbsp; | | Embedded PC,<br />HMI
&nbsp; | | Edge<br />Computing<br />Solution
&nbsp; | | Industrial PC<br />(Box, Rack)
ODM 서비스
기업정보 | 기업소개 | 하이버스 소개<br />/ 파트너/고객사<br />/ 특허/인증 및 수상
&nbsp; | 연혁
&nbsp; | 최신소식 | 목록 | | 프로그래밍<br />&nbsp;&nbsp;- 목록
&nbsp; | | 상세 | | 프로그래밍<br />&nbsp;&nbsp;- 상세
&nbsp; | 오시는길 | | | 프로그래밍<br />&nbsp;&nbsp;- 구글지도
고객지원 | 제품문의 | 목록 | | 프로그래밍<br />&nbsp;&nbsp;- 목록
&nbsp; | | 상세 | | 프로그래밍<br />&nbsp;&nbsp;- 상세
&nbsp; | | 등록 | | 프로그래밍<br />&nbsp;&nbsp;- 등록
&nbsp; | 기술지원 | 목록 | O | 프로그래밍<br />&nbsp;&nbsp;- 목록
&nbsp; | | 등록 | O | 프로그래밍<br />&nbsp;&nbsp;- 등록
&nbsp; | | 상세 | O | 프로그래밍<br />&nbsp;&nbsp;- 상세
&nbsp; | | 수정 | O | 프로그래밍<br />&nbsp;&nbsp;- 수정
&nbsp; | 다운로드 | 목록 | O | 프로그래밍<br />&nbsp;&nbsp;- 목록
&nbsp; | | 상세 | O | 프로그래밍<br />&nbsp;&nbsp;- 목록
&nbsp; | 연혁
My | 로그인 | | | 프로그래밍<br />&nbsp;&nbsp;- 로그인
&nbsp; | 회원가입 | | | 프로그래밍<br />&nbsp;&nbsp;- 중복확인
&nbsp; | | 개인정보 입력 | | 프로그래밍<br />&nbsp;&nbsp;- 등록<br />&nbsp;&nbsp;- 이메일 인증
&nbsp; | | 가입 완료
&nbsp; | 아이디찾기 | | | *아이디 찾기 방식 확인 필요
&nbsp; | 비밀번호찾기 | | | *비밀번호 찾기 방식 확인 필요
&nbsp; | 개인정보 확인/수정 | | O | 프로그래밍<br />&nbsp;&nbsp;- 상세/수정

## FrontEnd / 관리자

Depth 1 | Depth 2 | Depth 3 | desc
---|---|---|---
로그인
회원조회 | 목록
&nbsp; | 상세
게시판 관리 | 최신소식 | 목록
&nbsp; | &nbsp; | 상세
&nbsp; | &nbsp; | 수정/삭제
&nbsp;  | 제품문의 | 목록
&nbsp; | &nbsp; | 상세
&nbsp; | &nbsp; | 수정/삭제
&nbsp; | 기술지원 | 목록
&nbsp; | &nbsp; | 상세
&nbsp; | &nbsp; | 수정/답변/삭제
&nbsp; | 다운로드 | 목록
&nbsp; | &nbsp; | 상세
&nbsp; | &nbsp; | 등록
&nbsp; | &nbsp; | 수정/삭제

# 사용 기술

## FrontEnd(사용자/관리자)

- 개발언어 : javscript / typescript
- 프레임웤 : Node.js + Nuxt3

## BackEnd(API)

- DB: mariadb(mysql)
- 개발언어 : javscript / typescript
- 프레임웤 : Node.js + NestJS
